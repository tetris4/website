---
title: "Projects"
description: "Projects and organizations to which I've been contributing on a voluntary basis."
menu: main
weight: -268
---

>To see more details on my contributions and responsibilities in each project, please check out my [full resume](https://www.linkedin.com/in/neofytoskolokotronis/).

---

# *• Currently* 

## __[KDE](https://www.kde.org/)__
![](/images/kde64.png)  
An international free software community that develops Free and Open Source based software.

## [CHAOSS](https://chaoss.community/)
![](/images/chaoss64.png)  
A Linux Foundation project focused on creating analytics and metrics to help define community health.

## [ELLAK Cyprus](https://ellak.org.cy/)
![](/images/ellakcy64.png)  
A Free Software and Open Technologies registered society in Cyprus.   

---

# *• Formerly* 

## __[Chakra](https://www.chakralinux.org)__
![](/images/chakra64.png)  
A GNU/Linux distribution with an emphasis on KDE and Qt technologies and a unique half-rolling repository model.  

## __[Kontalk](https://kontalk.org/)__
![](/images/kontalk64.png)  
A distributed and encrypted instant messaging system.  

## __[Cynaxis](http://cynaxis.org/)__
![](/images/cynaxis64.png)  
An informal Cyprus-based initiative with a focus on improving civic access to public data.



