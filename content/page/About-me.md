---
title: "About"
description: "A short introduction of myself"
menu: main
weight: -269
---

![](/images/aboutme.jpg)

I studied medicine and got a degree in psychology, before deciding to follow my passion for technology.

Over the years, I have been connecting the dots between teams and projects, products and communities, and I have extensive hands-on expertise across this spectrum. 

I am a long-time contributor to international Open Source software projects and communities, and a founder of Open Data/Government initiatives in my home-country, Cyprus.

I enjoy working with both technical and non-technical contributors in getting things done, creating strong relationships and nurturing collaboration towards common goals.

I thrive when working in nimble teams, organizations with human-centered cultures, environments with diversity and projects developed in a forward-looking mindset.

> I created this website with love and [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software). It's built with [Hugo](https://gohugo.io/) using the [Minimo](https://minimo.netlify.app/) theme and it's hosted on [Gitlab](https://gitlab.com/tetris4/website).


