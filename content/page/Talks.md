---
title: "Talks"
description: "Presentations I have given over the years"
menu: main
weight: -267
---

## Building Culture

<iframe title="Building Culture: It Starts at Onboarding and Never Ends  - Akademy 2024" width="560" height="315" src="https://tube.kockatoo.org/videos/embed/82782bb5-3eb0-4018-a7c1-b643d9503888?warningTitle=0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

Event: [Akademy 2024](https://akademy.kde.org/2024), Würzburg, Germany  
Read [description](https://conf.kde.org/event/6/contributions/219/)  
Download [slides](https://conf.kde.org/event/6/contributions/219/attachments/125/159/NK-Building%20Culture%20(copy).pdf)


## The KDE Free Qt Foundation
{{< youtube CRIBij4koJI >}}  
Event: [Qt Greece 2023](https://qtgreece.extenly.com/), Athens


## Developing products that break out of our bubble(s)
{{< youtube z3gw-5WMGBU >}}  
Event: [Akademy 2021](https://akademy.kde.org/2021), Online  
Read [description](https://conf.kde.org/event/1/contributions/25/)  
Download [slides](https://conf.kde.org/event/1/contributions/25/attachments/15/16/Developing%20Products%20outside%20of%20our%20Bubble%28s%29%20-%20Neofytos%20Kolokotronis.pdf)


## Helping people make a living with KDE products
{{< youtube MzpoTuHuciE >}}
Event: [Akademy 2021](https://akademy.kde.org/2021), Online  
Read [description](https://conf.kde.org/event/1/contributions/29/)  
Download [slides](https://conf.kde.org/event/1/contributions/29/attachments/33/38/2P1T1%209%20neofytos%20and%20lydia%20Make%20a%20living%20around%20KDE.pdf)


## All on board! Are we there yet?

 <figure class="video_container">
<iframe width="700" height="395" sandbox="allow-same-origin allow-scripts" src="https://files.kde.org/akademy/2019/112-All_on_board_Are_we_there_yet.mp4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Event: [Akademy 2019](https://akademy.kde.org/2019), Milan, Italy  
Track: KDE Goals  
Read [description](https://conf.kde.org/en/akademy2019/public/events/112)  
Download [slides](https://conf.kde.org/system/event_attachments/attachments/000/000/114/original/Akademy_2019_NK.pdf?1569166630)


## Streamlined onboarding of new contributors
{{< youtube qidWtWGK3J4 >}}  
Event: [Akademy 2018](https://akademy.kde.org/2018), Vienna, Austria  
Track: KDE Goals  
Read [description](https://conf.kde.org/en/Akademy2018/public/events/7)  
Download [slides](https://conf.kde.org/system/event_attachments/attachments/000/000/013/original/OnboardingGoal_Akademy2018.pdf?1533827528)


## The Half Rolling repository model - The golden intersection for desktop users?
{{< youtube HgxNNUFTNLo >}}  
Event: [FOSDEM 2018](https://fosdem.org/2018/), Brussels, Belgium  
Track: Distributions devroom  
Read [description](https://fosdem.org/2018/schedule/event/half_rolling_repository_model/)  
Download [slides](https://fosdem.org/2018/schedule/event/half_rolling_repository_model/attachments/slides/2392/export/events/attachments/half_rolling_repository_model/slides/2392/Half_Rolling.pdf)


## Communication Applications - Why being FOSS is only a first step  
{{< youtube aNa1yRr4mjk >}}  
Event: [T-DOSE 2017](https://www.t-dose.org/2017/schedule), Eindhoven, The Netherlands  
Track: Keynote  
Read [description](https://www.t-dose.org/node/1079)  
Download [slides](https://old.t-dose.org/sites/t-dose.org/files/communication_applications.pdf)
