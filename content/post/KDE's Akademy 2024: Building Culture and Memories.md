---
title: "KDE's Akademy 2024: Building Culture and Memories"
description: "It starts at onboarding and never ends"
date: "2024-10-01"
categories:
  - "KDE"
tags:
  - "onboarding"
  - "community"
  - "akademy"
  - "talk"
---

# The event

Another year, another Akademy; and what an incredible experience it continues to be! 

KDE's Akademy 2024 was my 7th in a row (5 in-person and 2 online), and I’m continually struck by how much we all gain, both individually and as a community, from coming together at this annual event. The opportunity to connect with over 100 contributors from across the KDE ecosystem, all passionate about the future of our projects, is something truly special. It's fills you with energy to continue and do even more!

This year’s Akademy was based in the city of Würzburg, Germany. I wish I had more time to explore the local sights and socialize with my fellow KDE gearheads, but I unfortunately had to leave right after the main event. Between rehearsing my talk and the rainy weather, I was left with zero time to walk around the city and meet with people on personal terms. I was delighted though to learn a bit of the history of the city from fellow attendees that grew up or lived there at some point of their lives. In addition to not visiting the city, one of my biggest regrets was missing out on the BoF (Birds of a Feather) sessions, where some of the most insightful discussions and hands-on work happen. I hope to make up for it next year. 

For me, Akademy is not just a time for discussions on KDE’s technical progress but also a critical element in building and sustaining KDE’s culture. And Culture was the topic I chose to talk about in my presentation to fellow KDE contributors.


![](/images/akademy2024-groupphoto_crop_1500.jpg)

# Building Culture

I tried to summarize here the main topics I covered in my presentation, but I do urge you to watch the [video of it](http://neofytosk.com/talks/) if you are interested in the topic.

**The Role of Onboarding in Culture Building**

Onboarding new contributors is one of the most critical moments in shaping KDE’s culture. It’s the first experience someone has with our community, and it can set the tone for how they feel about contributing long-term. I’ve seen firsthand how positive onboarding experiences can leave newcomers feeling welcomed, supported, and encouraged to contribute more. However, onboarding doesn’t end after the first few weeks. Ongoing engagement and mentorship are vital to ensuring that contributors remain aligned with KDE’s values.

**Sustaining Culture Requires Effort**

Maintaining a strong culture requires active participation from everyone. We need to embody the values we promote, whether that’s through positive engagement or conflict resolution. KDE has always prided itself on being an inclusive and welcoming community, but it’s important to ensure that we continuously reflect on our practices and adjust where needed.

**Embracing Change While Staying True to Our Core Values**

KDE’s culture should be evolving as our community and technology do, but we also need to remain true to our core values, as they are described and documented in our Manifesto, our Code of Conduct, our Vision and our Goals. Whether it’s how we communicate, how we handle decision-making, or how we manage conflict, change is inevitable and we should be embracing it for our benefit. Balancing change with tradition is certainly not an easy task, and to tackle it I suggest an approach with four key steps: discuss ideas, reflect on experiences, nurture relationships, and reinforce values. 

**The Challenges We Face**

In any thriving community, challenges are inevitable, and KDE is no exception. One of the biggest obstacles we face is overcoming apathy. Without active participation, it’s difficult to maintain momentum, which can create a disconnection between our community’s values and our everyday actions. At the same time, navigating difficult topics and addressing toxic behavior require a delicate, thoughtful approach to avoid causing divisions within the community. This is a crucial focus for the [Community Working Group](https://ev.kde.org/workinggroups/cwg/), where nearly every decision triggers some level of reaction or feedback from members. As our community continues to grow, another challenge is ensuring that communication gaps are bridged, making sure that every voice—regardless of how loud—is heard and respected.

**Strategies For Tackling The Challenges**

I outlined four groups of strategies to address the challenges we face in maintaining KDE’s culture. First, establishing expectations and accountability is essential. This involves clearly defining behavior guidelines, outlining consequences for violations, and ensuring transparency in decision-making. Second, empowering and supporting the community through capacity building, providing training for moderators and contributors, supporting an active Community Working Group, and maintaining a zero-tolerance policy for toxic behavior. Third, fostering positive engagement and collaboration by highlighting positive behavior, creating opportunities for teamwork through joint projects, mentoring programs, and events, and promoting leadership by example. Finally, continuous improvement and adaptation ensures that we gather regular feedback, encourage inclusive participation, maintain a welcoming onboarding process, and address contributor burnout to keep KDE’s culture vibrant and evolving.

**Be a Culture Champion**

I closed my talk with a Call to Action, urging everyone in KDE to take ownership of our culture. Whether you’ve been around for years or are new to the community, your actions and interactions shape the future of KDE. Lead by example, contribute your ideas, and help us build a community where everyone feels welcome and valued.

And if you’re looking for ways to get more involved, consider joining the Community Working Group. We need more people to take an active role in supporting and nurturing KDE’s culture.

Let’s work together to ensure KDE continues to be a vibrant and welcoming community for years to come!

# KDE e.V. Board friends & memories

One of the fun and memorable moments at Akademy this year was gathering for a group photo with past members of the KDE e.V. Board. It was a nostalgic experience, reconnecting with people that I've worked closely with over my three years at the board, but also connecting with past members that have played pivotal roles in shaping KDE’s journey. 

![](/images/PXL_20240907_163748749.jpg)

# Looking Forward to Akademy 2025

As I reflect on my time at Akademy 2024, I’m reminded why I keep coming back. KDE isn’t just a collection of projects—it’s a community. Being here, surrounded by people who share the same vision and values, reinforces my belief that Akademy is essential for KDE’s long-term success. The hallway conversations I’ve had, the ideas we’ve shared, and the relationships we’ve built all contribute to the vibrant culture that makes KDE what it is.

For anyone who hasn’t attended Akademy, I highly encourage you to join next year. There's something irreplaceable about being at Akademy in person. It’s an experience that goes beyond technical discussions and code—it’s where KDE’s culture is lived and breathed. And that’s something worth being a part of.

Looking forward to the next one!


