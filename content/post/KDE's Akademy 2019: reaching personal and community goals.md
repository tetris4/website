 
---
title: "KDE's Akademy 2019: Reaching personal and community goals"
description: "Being onboarded to the board"
date: "2019-12-29"
categories:
  - "KDE"
  - "Chakra"
tags:
  - "onboarding"
  - "community"
  - "akademy"
  - "talk"
  - "sprint"
---

Akademy 2019 was in several aspects significant, both for me and the KDE community. This was my second time attending Akademy, and compared to my first Akademy in 2018, I  was now participating as a member of the [KDE e.V.](https://ev.kde.org/), a candidate for the [Board of Directors](https://ev.kde.org/corporate/board.php) and a goal keeper for the [Streamlined Onboarding](http://neofytosk.com/post/streamlined-onboarding-how-far-has-kde-come/) goal, presenting [a talk](https://conf.kde.org/en/akademy2019/public/events/112) on our progress and being part of a panel discussion with the other goal keepers. 

![](/images/Akademy2019-BannerDuomoMilan.jpg)


Below I try to summarize my thoughts, drawing memories from photos that I kept from the event.

# The conference

Akademy 2019 took place at the University of Milano-Bicocca in Milan, Italy, from Saturday the 7th to Friday the 13th of September.

Around 150 attendees participated, representing a global KDE community, to discuss and plan the future of the community and its technology. We were also very happy to host participants from other Free and  Open Source software projects and communities, in addition to representatives from the [Italian government](https://dot.kde.org/2019/09/05/developers-italia-and-new-guidelines-let-open-source-revolution-start-interview-leonardo), local organizations, [software companies](https://ev.kde.org/consultants.php), journalists,  and of course our [supporting members](https://ev.kde.org/supporting-members.php) and [sponsors](https://akademy.kde.org/2019/sponsors).

If you are interested, please check out the recommendations and links to [all the videos from the talks](https://dot.kde.org/2019/10/14/akademy-2019-talks-videos) that were delivered at the event.

![](/images/Akademy2019-group.jpg)

# KDE Goals

The first set of Goals were chosen by our community back in 2017, with the aim of giving KDE contributors focus in the years to follow, helping us improve our people and our software. 

As part of the Goals  initiative, 3 goals where chosen: 

- [Usability and Productivity](https://pointieststick.com/2019/06/05/kde-usability-and-productivity-are-we-there-yet/)
- [Privacy](https://dot.kde.org/2019/06/05/kde-privacy-sprint-2019-edition)
- [Onboarding of new contributors](http://neofytosk.com/post/streamlined-onboarding-how-far-has-kde-come/)

At Akademy 2019 we held a panel discussion with the rest of the goal keepers, aiming at taking a look at how far we've come and sharing our experiences and lessons learned from our first set of goals.

<iframe width="700" height="395" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/35d0ed32-b7e0-4020-974f-06bd265ffa16?warningTitle=0" frameborder="0" allowfullscreen></iframe>

With these goals being adopted nowadays by our community and already impacting the software we develop, we believed it was a good time to prioritize new KDE goals for the next 2 years, all of which were selected by our community through a voting process that involved dozens of proposals and hundreds of voters. 

The [new set of challenges](https://dot.kde.org/2019/09/07/kde-decides-three-new-challenges-wayland-consistency-and-apps) that we decided to address are:

- [Wayland](https://community.kde.org/Goals/Wayland) 
- [Consistency](https://community.kde.org/Goals/Consistency)
- [All about the Apps](https://community.kde.org/Goals/All_about_the_Apps)

![](/images/Akademy2019-goals1.jpg)<small>[sketchnotes](https://twitter.com/dr_ervin/status/1170286078307295234) by Kevin Ottens</small>

## Streamlined onboarding of new contributors

As a project coordinator for this goal, it was up to me to inform our community of everything that we have achieved over the past years that the KDE mother ship has been sailing towards the "Streamlined Onboarding" land. 

You can watch the [video of the talk](https://files.kde.org/akademy/2019/112-All_on_board_Are_we_there_yet.mp4) for all the details, download the [presentation slides](https://conf.kde.org/system/event_attachments/attachments/000/000/114/original/Akademy_2019_NK.pdf?1569166630) or get a quick overview from the sketch-notes below!

![](/images/Akademy2019-onboarding.jpg)<small>[sketchnotes](https://twitter.com/dr_ervin/status/1170278994836709377) by Kevin Ottens</small>

### KDE switching to Gitlab

Having been a member of the team responsible for coordinating our discussions with Gitlab regarding [KDE switching](https://about.gitlab.com/press/releases/2019-09-17-gitlab-adopted-by-KDE.html) some of its infrastructure to it, I was very happy that Gitlab not only attended Akademy, but was also one of our sponsors and organized a [workshop](https://twitter.com/rspaik/status/1171057767270047748) on getting people involved in their community.

We 've been working on getting the issues around this transition resolved, and thanks to the great work put it by the System Administration team, we are now closer than ever to fully adopting Gitlab and taking advantage of all the technological features it provides.

![](/images/Akademy2019-gitlab.jpg)

# Board of Directors of the KDE e.V.

[KDE e.V.](https://ev.kde.org/) is a registered non-profit organization that represents the KDE Community in legal and financial matters. The e.V.'s annual general meeting (AGM) takes place every year at Akademy, during which the reports on the activities of the Board of Directors and the working groups (System Administration, Financial, Community, Advisory Board, Fundraising) are presented. In addition, the election of new board members happens when its necessary. This was my first time attending an AGM as a member of the e.V., since last year I was invited to participate as a guest. 

As a new e.V. member, being a candidate for the [Board of Directors](https://ev.kde.org/corporate/board.php) was a huge step for me to take, making me feel simultaneously anxious and excited. Even though I've been involved in various KDE projects and activities ever since I joined as a contributor, I hadn't interacted in the past with many of the e.V's members, from whom I was asking their vote in order to join the Board of Directors. Over the summer I had thought a lot about submitting my candidacy. In the end, I decided to step up, as it felt as the logical next step from me. After a couple of years coordinating a community goal, I would be given a chance to contribute to KDE from a more strategic position. 

I was extremely happy and honored to be voted in as a member of the Board of Directors, especially when considering that rest of candidates were all very experienced KDE contributors. I would like to send again a big thank you to the members of the e.V. for their trust and a big hug to the members of the board for their warm welcome. As a member of the [new board](https://dot.kde.org/2019/09/27/meet-kde-evs-new-board), I get a unique opportunity to join an experienced team of fellow KDE contributors and multi-skilled professionals, taking on the responsibility of representing our community and its best interests, with the goal of helping KDE  move forward. 

![](/images/Akademy2019-board.jpg)

# Chakra's presence

My involvement in KDE doesn't allow for much extra time, so I am nowadays not very active in [Chakra](https://chakralinux.org/). I do consider myself a member of Chakra's community though, and it is always a pleasure to meet in-person with people from all over the world with whom I have collaborated online for so many years. I was especially delighted to see so many Chakra contributors attending an Akademy, with several of us already being active contributors to our beloved KDE. Such a shame I did not bring my Chakra t-shirt with me!

![](/images/Akademy2019-chakra.jpg)


Till [next Akademy](https://dot.kde.org/2019/12/27/host-akademy-2020-your-city)! Let us know if you would be interested in hosting it in your city. 

