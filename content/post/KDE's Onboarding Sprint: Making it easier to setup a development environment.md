---
title: "KDE's Onboarding Sprint: Making it easier to setup a development environment"
description: "Enabling newcomers to start coding on KDE software"
date: "2019-08-20"
categories:
  - "KDE"
tags:
  - "onboarding"
  - "community"
  - "sprint"
---

# The Megasprint 

Between the 19th and 23rd of July 2019, around 20 KDE contributors met in [Nuremberg](https://en.wikipedia.org/wiki/Nuremberg), Germany, to work on 3 different projects, in what turned out to be a KDE Megasprint:

1. [KDE Connect](https://community.kde.org/Sprints/KDE_Connect/2019), the amazing application that intuitively connects and integrates your mobile and desktop devices
2. [KWin](https://community.kde.org/Sprints/KWin/2019), Plasma Desktop's powerful window manager
3. [Streamlined Onboarding Goal](https://community.kde.org/Sprints/Onboarding/2019), focused on making it easy to setup a development environment.

There were so many KDE people around, split in teams, discussing and working on a variety of projects, that at times this felt like a mini conference and not a sprint! 

## The venue

[Suse](https://www.suse.com/) were generous enough to offer two spacious and fully equipped offices at their headquarters to host the KDE sprints. We owe a special thanks and a big KDE hug to the [OpenSuse](https://www.opensuse.org/) team and especially Douglas DeMaio and  Fabian Vogt for being incredible hosts.

![](/images/kdesrcbuild.jpg)


# The Streamlined Onboarding Goal Sprint 

This sprint took place as part of KDE's [Streamlined Onboarding goal](http://neofytosk.com/post/streamlined-onboarding-how-far-has-kde-come/) that has been running for a year and a half now. It's focus was on initiating the discussion and paving the path forward for [making it easier](https://phabricator.kde.org/T8484) for new contributors to setup a development environment and get started with developing KDE software.

My understanding is that the topic was discussed at various times in the past, but a final solution to proceed with was never decided upon. This can be justified to some extend by the challenges that are involved. [KDE](https://www.kde.org) is a community that hosts dozens of projects, that provide solutions and functionalities for a variety of tasks, ranging from simple to complex in terms of their codebase and extending from single-person maintained to having multiple collaborators.  Consequently, each project has different requirements and the learning curve for starting to contribute can be of varying difficulties. 

This was not an easy challenge to untangle to begin with and coming up with a "one size fits all" solution would definitely not be an effortless task, if at all possible.

# How we currently do things

To approach the topic, my suggestion was to split the sprint in two parts. During the first day, we sat around the room and had a long discussion on how we currently do things and the requirements for a future solution.

The [current process](https://community.kde.org/Get_Involved/development) that we recommend to newcomers utilizes [kdesrc-build](https://kdesrc-build.kde.org/), a command line tool that has served us well up to now by automating the process of downloading, managing and building KDE source code repositories. I was very happy that even though Michael Pyne, kdesrc-build's maintainer, was not able to attend the sprint, he put together a talk that he delivered to us via a video call. In the call, that is now [available for everyone](https://cdn.kde.org/onboarding/2019-07-23-kdesrc-build-brief-onboarding-sprint.mp4) to watch, he presented how kdesrc-build works, covering the advantages it has and the challenges it needs to overcome.

What became clear early on was the gap that exists between the process we currently ask newcomers to follow and the process which experienced KDE developers, like many of those attending the sprint, actually use on their own systems for developing KDE software. My understanding is that after gaining enough knowledge of the processes involved, developers end up implementing  their own custom ways to suit their workflows. This is not necessarily bad, as we want people to be able and capable enough to use solutions that suit their needs and increase their productivity. Yet, to serve the needs of a newcomer, we need to decide on a standard path and provide a solution that will be sustainable, easy to setup and simple to support.

# Identify use cases 

The next step was to list, compare and contrast all the ways that are available to developers at the moment in order to get started with coding on KDE software.

We considered implementations involving distribution packages, cmake external projects, docker built with dependencies from binary factory, docker with kdesrc-build, kdesrc-build, craft and flatpaks. 

It was quickly evident that finding a solution that would fit everyone's needs would be very hard, if not impossible. To be able to proceed, we had to narrow it down to 3 major use cases:

1. [Applications](https://www.kde.org/applications/) (including [Frameworks](https://kde.org/products/frameworks/))
 - Self-contained (e.g. Krita)
 - Using multiple repositories (e.g. PIM)
2. [Plasma-Desktop](https://kde.org/plasma-desktop) 
3. [Plasma-Mobile](https://plasma-mobile.org/overview/)

The prevailing impression was that each use case would require a different approach for setting up a development environment. However, as the goal of the sprint was to enable new contributors and particularly those that would not have much experience with KDE's development process and tools, we decided that we would focus on use case #1. KDE's Applications, and self-contained ones in particular, are a great entry point for new people to get involved, and that is where we should be pointing newcomers that are just getting started. Hence, this is the case we should be tackling first.

The assumption we made is that more experienced developers would not mind getting their hands dirty with the complex setups that the rest of use cases demand, without this meaning in any way that improvements to simplify and optimize the process for those cases are not welcome.

![](/images/thedavids.jpg)<small> How many Davids does it take to solve an issue? </small>

# The requirements 

The discussion that followed revolved around the design and the challenges that a good solution would solve, as it should be able to:

- resolve the **dependencies**, by providing a set of pre-built binary dependencies that is as rich as possible. 
- configure the **tools** developers need to pre-install before they can get started (e.g. a compiler, git, cmake).
- avoid using a command line interface and offer **IDE integration**.
- download the **sources**, and then **build** and **run** the relevant application.

The discussion came down to the story that we wish to tell to developers that are getting started. What is the path we want them to walk on in order to start contributing code? Do we want Linux-based operating systems to be our go-to OS for development? Will we ask newcomers to use KDevelop as the go-to IDE? How do developers download a project and setup environment variables? 

Several of those questions where not given a clear answer. To be able to have a developer story, we need to offer a solid starting experience and make some choices that even though they might have limitations, they would best serve the purpose. 

# Work on prototypes for possible solutions

The result of the discussions that took place in the first day was that the two most feasible solutions would involve the usage of either [Conan](https://conan.io/) or [Flatpak](https://flatpak.org/), as they seemed to give answers to the emerging question of automating the resolving of dependencies, in addition to other features that each offered.  

In the second day the participating developers formed smaller groups in order to test the adoption of each of these solution to our needs. Most of the work went into using each tool to build a working prototype that would help with getting started with coding a KDE application on it. A small and simple KDE application (kruler) with not many dependencies was chosen for this purpose.

Conan is an open source C/C++ package manager for developers. Ovidiu-Florin Bogdan has significant experience with it, so he was able to explain the available features, respond to questions and share his insight on a possible implementation. Together with Dmitry Kazakov from the Krita team they worked on preparing a conan solution and documenting the first steps and challenges that this involved. 

Flatpak is a utility for building and deploying software and managing packages on Linux-based operating systems. Aleix Pol has put time in the recent past towards supporting this technology in KDevelop, but there is still work left to be done. Together with Albert Astals Cid and David Redondo they worked on testing the status of this implementation and record the pending challenges which we have to address if we want to bring this solution to a state that would serve our needs.

I'll avoid getting further into the technical aspects, as it is outside the scope of this post. If you are interested in a more detailed presentation of what was discussed at the sprint and the development work that took place you can do some further reading on the blog posts by [Albert Astals Cid](https://tsdgeos.blogspot.com/2019/07/my-kde-onboarding-sprint-2019-report.html) and [Boudewijn Rempt](https://valdyas.org/fading/kde/kde-onboarding-sprint-report/). 

# Going forward

The sprint helped in kick-starting the discussion about making it easy for newcomers to setup a development environment. 
The majority of KDE developers that attended the sprint seemed to understood the importance of achieving this objective, but at the same time were concerned about the difficulties that such a task involves.

The major challenge going forward will be to support the developers to put further work into the prototypes so we can have a fully working solution to test and put to use to our advantage.

[Akademy 2019](https://akademy.kde.org/2019), KDE's annual conference, is taking place in a few weeks in Milan, Italy. 
It will be a great opportunity to hold workshops on this topic to build upon the discussion and work that resulted from this sprint. If you are interested to contribute to this effort, do keep an eye on the [schedule](https://akademy.kde.org/2019/program) and join the relevant workshop.
Provided that the right momentum is gained, we could follow up with a second sprint that would get any chosen solution at a production-ready level.

![](/images/plasmaonsteamvr.jpg)<small>KolourPaint in VR!</small>

# In a parallel reality

As mentioned in the beginning of this post, the Megasprint was not only about the Streamlined Onboarding goal. 

Due to my involvement in coordinating the onboarding goal activities that took place, I could not follow much of what went on with the other sprints.To learn more on the progress made there, make sure to check out the three-part blog post by [Simon Redman](https://simonredman.wordpress.com/2019/07/25/welcome-to-kde-nuremberg-megaspring-part-1/) on KDE Connect and [Roman Gilg](https://subdiff.org/blog/2019/kde-sprints-in-summer-heat/#kwin-sprint)'s blog post on Kwin.

Last but not least, I was excited to have a unique opportunity to play around with Virtual Reality technology using [Steam VR](https://www.steamvr.com/en/). It wasn't the first time I was experiencing VR technology, but what made it feel fascinating this time was that I was able to experience a "minority report" type of interacting with the Plasma Desktop environment; moving around windows, zooming in and out, and using applications via the controllers. If this caught your attention, do not miss [Christoph Haag's talk](https://conf.kde.org/en/akademy2019/public/events/137) at Akademy, as he will be presenting a proposal on integrating VR with Plasma!


To close this post, I would like to say a big thank you to all the KDE developers that joined us in this Megasprint. See you all at Akademy 2019! =)

_____
*Got a comment? Feel free to share your thoughts on the corresponding [twitter](https://twitter.com/tetris4/status/1163898399613181952) and [reddit](https://www.reddit.com/r/kde/comments/ct4tth/kdes_onboarding_sprint_making_it_easier_to_setup/) posts. =)*
