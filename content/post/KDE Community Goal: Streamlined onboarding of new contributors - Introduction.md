---
title: "KDE Community Goal: Streamlined onboarding of new contributors"
description: "Part 1: Introduction"
date: "2018-01-03"
categories:
  - "KDE"
tags:
  - "onboarding"
  - "community"
---

# Community Goals

Over the second half of 2017, KDE has been going through the ambitious effort of having its community propose and [choose goals](http://blog.lydiapintscher.de/2017/08/20/evolving-kde-lets-set-some-goals/) for the next 3-4 years.

[These goals have been set](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond) now, and I was thrilled to learn that my proposal on __[Streamlined onboarding of 
new contributors](https://phabricator.kde.org/T7116)__ was chosen and many other KDE contributors believed this was a goal worth pursuing in the near future and voted for it. 

The other two proposals that were selected are __[Top-notch Usability and Productivity for Basic 
Software](https://pointieststick.wordpress.com/2017/12/25/kde-goal-usability-and-productivity/)__ and __[Privacy](https://vizzzion.org/blog/2017/11/kdes-goal-privacy/)__.

![](/images/ideas.jpg)<small>[image](https://www.flickr.com/photos/opensourceway/7496802920/in/album-72157626295143856/) by opensource.com</small>

# Background

Due to my involvement in [Chakra](https://www.chakralinux.org/
) I have been closely following KDE and using its software for nearly a decade. But other than discussing with developers, reporting bugs, translating and giving support to users, one could say I wasn’t directly involved in the community and its projects, at least not until I recently joined the [Promo team](https://community.kde.org/Promo).

I felt like this goal setting process was a great opportunity for me to step up, prepare a proposal and share my ideas with the rest of KDE. A big part of what I do for software projects is keeping an eye on end users and striving to find ways to enable people to become active and contribute their part in the areas and causes they care about. Therefore I greatly appreciated the initiative as it facilitated a bottom up decision making process that allowed for the voice of the wider community to be heard and acted upon. 

As a non coder, I have been looking for ways to get involved in KDE for a while now. I have tried asking in related channels, contacting other contributors directly and going through the documentation on the website. People were always polite with me, but I never felt that someone stopped to offer their time and effort to get me involved or pointed me towards a clear path to follow based on my experience and skills. 

# The Proposal

In my proposal I inevitably focused upon the KDE community itself and the procedures involved. Specifically on how to standardize and enhance the getting involved process. 

KDE is to a large extend a great example of a self-organized community. We tend to take for granted that potential contributors will find their way around one way or another. It’s indeed great when self-driven determined individuals are willing to invest time and effort into finding what suits them best and discovering how they can contribute. Yet I strongly believe we can make this easier and more straightforward for them. We can be more proactive in enabling people that might be shy, less confident or in need of guidance. The aim is to find ways to lower the barrier for persons that are interested to get involved and nurture the growth of the community in terms of gaining more contributors and reaching its full potential.

In my proposal, I tried to address these challenges. Among the suggested solutions were ideas such as improving documentation and increasing its visibility on the website, allocating contact persons per project or team and mentoring. It was very satisfying to see several people contributed their own thoughts on enhancing this task and further ideas have been added since: better bug tracking, easier coding frameworks for applications, even new ways to get new financial contributions.

KDE is foremost a software developing community, so that will be the main area of interest, but my intention here is for us to further increase inclusiveness for all types of contributions. The objective is to gain committed and happy new contributors by making the whole process more attractive and raising the quality of their onboarding experience. 

![](/images/paths.png)<small>[image](https://www.flickr.com/photos/opensourceway/4929153543/in/album-72157623343013541/) by opensource.com</small>

# Going forward

In addition to suggesting solutions, I considered it important to include in the proposal some thoughts on how to approach the implementation. The first step that made sense to me is _research_. We need to learn as much as we can on KDE's projects, the connections and the key people involved, so we can come up with solutions that will beneficial to everyone.

I am at the moment gathering all the feedback -including suggestions on the Phabricator task, comments on Reddit and IRC discussions- and I will then work on compiling them into something with more structure that can be used as a basis to launch this effort. 

I will soon contact the people that already reported their interest in helping with this effort so we can collaborate on setting the path to follow and allocate tasks accordingly. A sprint dedicated to this initiative is also planned to be organized to get those interested together in person to work on the proposal. 

I'll make sure to post regular updates as we move on to keep everyone informed. 

# Join our effort

If you are interested in giving a hand, we would be happy to hear from you! As you might have noticed, this website is very new so I unfortunately did not setup a comments section yet. Feel free to contact me directly with feedback or suggestions following the links at the footer of this page. If you already have a KDE account, comments directly on the [Phabricator task](https://phabricator.kde.org/T7116) are more than welcome.
