---
title: "An update on KDE's Streamlined Onboarding Goal, Akademy talk and first sprint"
description: "Part 2: The first steps"
date: "2018-07-12"
categories:
  - "KDE"
tags:
  - "onboarding"
  - "community"
  - "akademy"
  - "talk"
  - "sprint"
---

# The story

As I described in the [introductory post](http://neofytosk.com/post/kde-community-goal-streamlined-onboarding-of-new-contributors---introduction/), [KDE](https://www.kde.org/) has been working towards a trinity of goals and I have been responsible for pushing forward the [Streamlined onboarding of new contributors](https://phabricator.kde.org/T7116) one. 

Half a year has passed since my initial blog post and with [Akademy](https://akademy.kde.org/), KDE's annual conference, coming up in a month this is a great time to post a quick update on related developments.

# Progress so far

Over the past months I tried to organize some key objectives related to this goal, which sounded ambitious to begin with. On top of that, the more I learned on the procedures and management of such a diverse and decentralized community as KDE, the harder it looked to to be able to mobilize and inspire such an effort. To have a common reference point, I find it important to set a few core overarching objectives that contribute to the final goal, which you can find as sub-tasks on the initial task of the proposal. To sum them up, some key aspects that we could improve are:

- Making participation to KDE attractive to newcomers
- Being great at welcoming and guiding new members
- Having clear paths for getting people involved
- Increasing interaction between newcomers and more experienced contributors

![](/images/carmountainroad.jpg)<small>[image](https://unsplash.com/photos/yA5rU4_WN3I) by Nuno Silva</small>
 
It's important to note here that everything that was included in my initial proposal was based on my personal subjective experience, and even though the voting for this proposal validated to some extent my assumptions, I would wish for us to be more data-driven going forward. 
 
Even though I received some scattered feedback ever since this goal was publicized, I do plan to reach out to new contributors and hold short interviews with them, aiming at gathering some qualitative [feedback](https://phabricator.kde.org/T8711) and gaining a better understanding of their experience, their progress in integrating with the community, what they found appealing and the difficulties they came across. Based on this data, my wish is to form a quantitative survey and run it via our community, hoping to further improve our insight on what our community finds important to work on, allowing us to prioritize our objectives accordingly.
 
There is however important work that is already being done, as several KDE projects and contributors are beginning to take this goal into consideration and to my enjoyment, making steps to improve the onboarding procedures involved. On the documentation front, the [Get Involved](https://community.kde.org/Get_Involved) page has been reworked a bit to make it simpler and to the point, and make sure it is up to date. We would like to avoid duplication of information and long, complicated tutorials, unless otherwise necessary. The documentation for [Phabricator registration](https://phabricator.kde.org/T7646) also received some improvements.

Nicolas Fella [posted](https://nicolasfella.wordpress.com/2018/05/13/kde-connect-junior-jobs/) about working to distinguish among junior jobs for [KDEConnect](https://community.kde.org/KDEConnect), in order to make it easier for future contributors to pick a task they could complete as their first contribution. The idea is "to provide structured information for each task that helps you diving into it". His post inspired the [Elisa](https://community.kde.org/Elisa) team as well, and as Matthieu Gallien [shared](https://mgallienkde.wordpress.com/2018/05/16/news-about-elisa/), they are also taking up a similar initiative. 

The [Plasma Mobile](https://www.plasma-mobile.org/) team has been very active on this goal. They have prepared a dedicated webpage with an [interactive guide](https://www.plasma-mobile.org/findyourway/) on getting involved, and are working on a comprehensive guide for setting up a [development environment](https://community.kde.org/Plasma/Mobile/DevGuide) to make it straightforward for coders to start developing for the Plasma Mobile platform.
Effort has been put here as well on identifying junior jobs and revamping the descriptions of Phabricator tasks to make them friendly for newcomers by including relevant useful details such as: 

- Short and to the point description
- Suggested skill requirements
- Links to related technical resources

It would be awesome to see more KDE teams adopting similar initiatives. If you are part of a project that's already working on this goal and I missed mentioning you here, I would love for you to contact me so I can learn more about your efforts, perhaps we can use your feedback to help other projects as well.

![](/images/compassmap.jpg)<small>[image](https://unsplash.com/photos/ioYwosPYC0U) by Daniil Silantev</small>

# Akademy talk

KDE is one of the largest international communities that develop Free and Open Source Software and Akademy is where this community meets on an annual basis. [Akademy 2018](https://akademy.kde.org/2018) will take place in Vienna, Austria, from Saturday 11th to Friday 17th August and I have several reasons to look forward to it.

For starters, I'm excited to be giving a [talk](https://conf.kde.org/en/Akademy2018/public/events/7) at the event to present and discuss the "Streamlined onboarding of new contributors" goal to the participants. The talk is currently scheduled for Saturday 11th at 11:00 and if you are attending, I would be glad to see you there. My goal is not only to quickly explain what this goal is about and what we did so far towards it, but most importantly to try and inspire more KDE contributors to care more about involving new people into the projects they are participating in.

But there is definitely more to this than the talk. It will be my first time attending an Akademy event, so it's a great chance to meet with fellow contributors and gain further insight into the KDE community and its procedures. I always love the process of meeting in person with individuals who I've followed or interacted with in the past online. And of course, when meeting new people from such diverse backgrounds, coming from all over the world, so many fascinating stories and perspectives emerge. In addition, it is a great opportunity to get involved in the discussions, receive [professional training](https://community.kde.org/Akademy/2018#Trainings) on topics such as non-violent communication, online fundraising and campaigning, and documentation writing and participate in workshops with the various teams and projects. Finally, I've never been to Vienna or Austria, so I hope I can get a chance to have a quick glimpse at the city and the local culture while there.

# First sprint

KDE is a complex organization with so many different projects running under its umbrella and all sorts of skills required. But the truth is, a big majority of the community are software developers. These are the people that write the code, find and fix bugs, maintain projects.

For the first sprint of the Streamilined Onboarding Goal, we decided it would be good to focus on improving one specific aspect of the onboarding process, and for this time we chose to work on making it easier and faster for newcomers to set up a development environment. The current plan is for the event to take place in Athens, Greece, sometime in Autumn 2018 and I have already started looking for a place to host us. If you are interested to participate and believe you have the skills to contribute to this objective, please leave your name on the sprint-related [task](https://phabricator.kde.org/T8623).

# Join our effort

If you appreciate what KDE is doing and would like to step up and give a hand, we would be happy to hear from you! Feel free to contact me directly with feedback or suggestions following the links on the footer of this page. 

_____

*Got a comment? Feel free to share your thoughts on the corresponding [twitter](https://twitter.com/tetris4/status/1017458001123446785) and [reddit](https://www.reddit.com/r/kde/comments/91qduk/an_update_on_kdes_streamlined_onboarding_goal/) posts. =)*
