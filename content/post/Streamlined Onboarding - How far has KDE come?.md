---
title: "Streamlined Onboarding - How far has KDE come?"
description: "All onboard! Are we there yet?"
date: "2019-05-25"
categories:
  - "KDE"
tags:
  - "onboarding"
  - "community"
  - "sprint"
---

The KDE mothership has been sailing towards the "Streamlined Onboarding" land for almost 2 years now. It has been a long trip, with its ups and downs, hurdles and joys. 

# Set sail

When I first proposed the idea for this goal, the  destination felt being so far ahead, if ever reachable.  I could have not have imagined that it would be voted in by the community, adopted and worked on collectively. It was a trip that the KDE community decided to take together. 

At times I was wondering: Where do we begin? Do we have enough people onboard? Where should we be heading next? Are we moving toward the right direction? Are we moving at all?

![](/images/setsail.jpg)<small>[image](https://unsplash.com/photos/7ghPaPLdmTY) by Bobby Burch on Unsplash</small>

# Through the storms 

Perhaps many of you, including myself, often feel like we are not doing enough, both on a personal level and as a collective. We set so many objectives to begin with, we often struggled to stay focused on them, and the to-do list just keeps growing.

For people constantly  pondering about how procedures can improve and striving to find solutions and optimizations, it can be difficult to admit that we have reached some major milestones. 

Yet, we have achieved a lot together over the last couple of years. It was immensely satisfying to go through the process of gathering our achievements and realizing how far we have come. 

# Land ahoy!

The goal is closer that it has ever been! Let's look at some of the objectives we reached, with the hope that all of you KDE contributors will share my excitement and satisfaction as you read through them.

## Community

- When the audience was asked regarding their presence during [Akademy](https://akademy.kde.org/) 2018, everyone was amazed that nearly 1 in 2 participants were new contributors. Many of them were not only newcomers, but also young contributors. I was very proud that our community and the organization behind it could support them to attend our summit. It demonstrates that we are putting some of the money we are raising to good use, by returning them directly to our community and enabling our contributors to come together in productive ways. Not surprisingly, following Akademy 2018, there was a notable increase in new [KDE e.V.](https://ev.kde.org/) members' registrations compared to the previous year.

- We formed a [KDE Welcome team](https://webchat.kde.org/#/room/#kde-welcome:kde.org
), aiming to act as the first contact for people interested to contribute to KDE, responding to newcomers questions, offering them the much needed help to go through their first steps of getting involved and linking them with the various KDE projects and teams. If you want to help guiding newcomers around, do join us there! 

## Tools

- For several months now KDE is discussing, evaluating and testing a [migration to Gitlab CE](https://gitlab.com/gitlab-org/gitlab-ce/issues/53206). We are now at the final stages of the process and the results will soon be shared with the community so we can all decide if we want to go ahead with it.
If voted in, we hope that GitLab will prove to offer a variety of features that will improve our community's workflow and will lower the barrier for new contributors joining us. It is a tool widely used, using a process that most contributors are nowadays accustomed to, so it should make it significantly easier for them to get started. If you want you can already check it out yourself at our [self-hosted server](https://invent.kde.org). 

- KDE now has it's own [matrix instance](https://webchat.kde.org/#/welcome
), gaining a go-to medium for communications and gathering our community channels under one roof. Here you can chat with other members of the KDE community, join the rooms of specific teams or projects you are interested to follow and contribute to and receive help and guidance. 

- [Phabricator](https://phabricator.kde.org/T7646
) and [Bugzilla](https://phabricator.kde.org/T6832) improvements were implemented early on, in order to simplify the registration process and provide a more straightforward experience. We also set up a [Junior jobs](https://community.kde.org/KDE/Junior_Jobs) tag for people looking for easy and simple ways to get started with contributing to their project of interest.

## Documentation

- As with any software project, great documentation is hard to find, even though everyone constantly stresses its importance. The [Get Involved](https://community.kde.org/Get_Involved) page on our community wiki has been updated and is actively maintained to successfully guide newcomers to the projects they would like to contribute in and provide them with the necessary information and documentation to get started.
 
- Furthermore, a documentation specialist has been hired in order to assess the state of our documentation, prioritize our needs and pave the path to follow toward improving and keeping them up to date. 

 ![](/images/sailing.jpg)<small>[image](https://unsplash.com/photos/58AiTToabyE) by Karla Car on Unsplash</small>

# One harbor at a time

We have achieved a lot, yet there is always room for improvement. We are already planning the [Onboarding sprint](https://community.kde.org/Sprints/Onboarding/2019) to work on making it easier for newcomers to set up a development environment and start coding on KDE projects. We also believe that our [website](https://phabricator.kde.org/T9776) could be improved to offer a simplified step by step guidance to newcomers.

For me, the biggest achievement of the Onboarding goal is that it has been adopted by the wider community as a long term target and it is an aspect that contributors nowadays consider when making decisions within their projects.

I am pleasantly surprised every time I read in email threads or tasks that "this would be great to do as part of the onboarding goal" and to see team members asking "what can we do to improve the process for onboarding newcomers to our project?".

I can't help but often ask myself if this goal will ever be concluded. Will we ever reach the land of "Streamlined Onboarding"? The answer is probably no; we should always try to improve our onboarding processes. Yet, inevitably this goal will have to make room for [new ones](http://blog.lydiapintscher.de/2019/06/09/evolving-kde-lets-set-some-new-goals-for-kde/), as we collectively decide on novel lands to explore! 

Let's try our best to keep KDE a sustainable community that individuals love being part of, that continues to thrive and create products that people enjoy using.

_____
*Got a comment? Feel free to share your thoughts on the corresponding [twitter](https://twitter.com/tetris4/status/1137744274089492481) and [reddit](https://www.reddit.com/r/kde/comments/byl8ma/streamlined_onboarding_how_far_has_kde_come/) posts. =)*
