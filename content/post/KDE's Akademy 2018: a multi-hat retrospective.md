---
title: "KDE's Akademy 2018: a multi-hat retrospective"
description: "Looking back while moving forward"
date: "2018-10-12"
categories:
  - "KDE"
  - "Chakra"
tags:
  - "onboarding"
  - "community"
  - "akademy"
  - "talk"
  - "sprint"
---


It's been two months already since [Akademy 2018](https://akademy.kde.org/2018) took place and unfortunately due to life changes I've only just now managed to sit down and write something about it. Perhaps one reason that this took me so long is that I needed this time to process and absorb all the new information and experiences I was exposed to during my time in Vienna.  

I left Akademy with so much new information, a long list of to-dos, feeling overwhelmed but at the same time extremely motivated to start working on things. Below I try to present this experience from the various perspectives and the multiple identities I enacted while there.

In the meantime, I was so impressed by the community and its vision, that I didn't think twice about joining [KDE e.V](https://ev.kde.org/) as a member, so I can further offer my help in a more official manner.

# KDE Hat

## Onboarding Goal

Going into Akademy, my main goal was to get people inspired and start considering ways of making it easier for new contributors to join their KDE projects.

I was positively surprised to see this goal being well received by people and teams. During the event, but also in the weeks that followed, I was contacted by teams and individuals that were interested in improving on this area. Furthermore, I was contacted by individuals, both new and older contributors, willing to put effort towards this goal.

### The Talk

I was fortunate enough to present my [talk](http://neofytosk.com/talks/) very early in the conference, which allowed me to introduce myself as a newcomer to the community and the goal we have been working on. This enabled other participants to approach me in the following days of the event. To my surprise, the people that I had a chance to talk to and discuss over all things KDE and onboarding ranged from newcomers, contributors that have been around for a several years in KDE, but also people that were participating in the event but represented other projects from the FOSS ecosystem.

Another astonishing fact about this year's Akademy was the fact that half of the participants there where new KDE contributors. And this was evident across all rooms and in the halls, where you could see new faces and especially lots of young people and students.

### The BoF 

In addition to the talk, I hosted a Birds of Feather ([BoF](https://en.wikipedia.org/wiki/Birds_of_a_feather_(computing))) type of meeting where everyone interested in the goal could participate. The discussion revolved mainly around the paths through which people can get involved into KDE. We distinguished two opposite ways (centralized and decentralized) through which new people might reach KDE and begin their contributions and considered solutions for each one. 

The centralized way is referring to when people approach KDE through the various channels of communication without having a clear idea on the project or the way they want to get involved. Forming a [welcome team](https://phabricator.kde.org/T8712) could prove beneficial here, a group of people responsible for guiding newcomers through their first steps, introducing them to the right people and projects and making sure we are making the most of the skills they bring to the table. Also, we considered [redesigning](https://phabricator.kde.org/T9776) our Get Involved landing page, in a way that would offer newcomers quick and valuable information regarding the KDE projects they could start contributing to. 
 
The decentralized path has more to do with how each KDE project and team can increase their chances of attracting newcomers and be prepared for when they arrive. This includes topics such as having good and up to date documentation that is short and to the point, identifying contributors willing to act as mentors and tagging some [junior jobs] for newcomers to pick up and get started, but also paving the way for non-coders to become members of their teams.

![](/images/helpinghands.jpg)<small>[image](https://www.flickr.com/photos/9458417@N03/17479301901/) by Christian Siedler</small>

## Training

In this years' Akademy, participants were given the chance to receive [training](https://community.kde.org/Akademy/2018#Trainings) on a variety of topics from distinguished individuals in their fields. I took part in the trainings on "Online Fundraising and Campaigning" and on "Documentation". Through both I gained very valuable insights that I plan to put in use for the best of KDE, but could also very easily be transferred to any professional setting.

My understanding is that this was the first time that something like this was organized in an Akademy. For what it's worth, the vast majority of the people I asked were very happy to participate in these sessions and expressed their eagerness to join future trainings as well. So it looks like I am not alone in looking forward to the next set.

## Plasma Mobile

At Akademy we managed to hold a couple of impromptu meetings with the [Plasma Mobile](https://www.plasma-mobile.org/) team that proved very productive. The main goal was to set a path to drive us forward and to set out the key milestones for the next release. It was quite satisfying to see a team of very young contributors willing to put work into this.

With the team growing around this project, we are now in the process of arranging a Plasma Mobile [Sprint](https://phabricator.kde.org/T9729) where contributors can get together and work on KDE's attempt to bring the Plasma Desktop to mobile phones. 

# Chakra Hat

As part of the [distros' Birds of a Feather](https://community.kde.org/Akademy/2018/KDE_Distro_BoF_Lightning_Talks) discussion, I presented [Chakra](https://chakralinux.org) with a quick talk on our past and present as a KDE-focused distribution. I briefly introduced Chakra's selling points; our very close relationship with KDE over the years, our ArchLinux legacy, and the unique half-rolling release model.  This is the second time I represent Chakra at an event, and it was interesting that again most of the questions I was asked were about the half-rolling repository system and how we manage the package updates. 

I also got to meet with a fellow Chakra and KDE contributor from Taiwan, [Jeff Huang](https://twitter.com/s8321414), after collaborating with him for so many years online. This is always such a special feeling. The team behind Chakra is small and most of the current and past contributors are also contributing to KDE. Jeff is already the 6th Chakra contributor I meet in person; hopefully I get to meet even more in the years to come.

![](/images/akademy2018-groupphoto.jpg)<small>[image](https://devel-home.kde.org/~duffus/akademy/2018/groupphoto/) by KDE</small>

# Community Manager Hat

Over the years I have been interacting with hundreds of people from countries all over the world on a daily basis through my participation in open source projects. Even though I make heavy use of a series of online tools, both synchronous and asynchronous, in order to achieve a high level of collaboration and communication, I can confidently say that no other form of interaction we currently have available can replace meeting with other people in person. In addition to this helping immensely with building trust, revealing intentions and nurturing relationships and friendships, it also drives innovation and participation and acts as a catalyst for brainstorming and decision making within projects. 

I got a chance to receive feedback in real time, got to test websites and gather opinions while sitting next to real life users of our product and participated to all sorts of brainstorming in regards to features, future plans and tactics. I can't imagine how one would not get inspired by all the things happening around you at events like Akademy.

This is what building and interacting with communities is all about for me; diversity in participation, open communication, freely exchanging ideas, sharing information, and most importantly cultivating relationships.

# Free Software Hat

This is one aspect that I didn't predict, yet during my time in Vienna, and even though this was a KDE-specific event, I got to connect with people from various open source projects. I held very informative discussions with contributors from Debian, Gnome, Mozilla, PostmarketOS and Wikimedia, among other smaller projects. Many FOSS contributors, myself included, tend to spend their time and resources among a variety of projects and use software from various open source vendors. The heated debates weren't missing, but the willingness to exchange ideas and converge where possible was also there and that left me optimistic on the future.


# Traveler Hat

It is always impressive to come across such diverse communities as KDE. Within days you get a chance to interact with people from all over the world. When I was visiting Vienna, my intention was to save some time to go around the city and get a glimpse of the local culture, by walking among the streets, hanging around festivals and visiting a few of the many museums located in the capital of Austria. I did do most of that to the extend possible, but on top of it, through these few days spent there, I got to learn so much about so many countries, by meeting and discussing with all the KDE contributors that came from around the globe.

I've had lots of interesting discussions from people from Brazil, USA, India, Spain, Germany and got a chance to be informed about local customs, cultures, food and music. This is always a wonderful side-effect of attending such conferences and I can never have enough of such experiences.


All in all, if you are or plan to be a KDE contributor, attending Akademy is definitely a must to get acquainted with the community. I am sure you will leave at least as excited about it as I did! 

I can't wait for the next one. 

_____
*Got a comment? Feel free to share your thoughts on the corresponding [twitter](https://twitter.com/tetris4/status/1056628190662352901) and [reddit](https://www.reddit.com/r/kde/comments/9s5xo4/kdes_akademy_2018_a_multihat_retrospective/) posts. =)*


