---
title: Home
menu: main
weight: -270
---

![](/images/profile-right-circle-small.png)

> Learn a bit more [about me](http://neofytosk.com/about/).  
> Check out my [professional resume](https://www.linkedin.com/in/neofytoskolokotronis/) and connect with me.  
> Have a look at the [projects](http://neofytosk.com/projects/) I've been involved.  
> Watch some of the [talks](http://neofytosk.com/talks/) I've given over the years.  
> Contact me via [email](mailto:neofytosk@posteo.net).  


